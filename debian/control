Source: golang-github-muhammadmuzzammil1998-jsonc
Section: golang
Priority: optional
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Anthony Fok <foka@debian.org>
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-sequence-golang,
               golang-any
Testsuite: autopkgtest-pkg-go
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-muhammadmuzzammil1998-jsonc
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-muhammadmuzzammil1998-jsonc.git
Homepage: https://github.com/muhammadmuzzammil1998/jsonc
XS-Go-Import-Path: github.com/muhammadmuzzammil1998/jsonc

Package: golang-github-muhammadmuzzammil1998-jsonc-dev
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: JSON with comments for Go!
 JSONC is a superset of JSON which supports comments.  JSON formatted
 files are readable to humans but the lack of comments decreases
 readability.  With JSONC, you can use block (/* */) and single line (//)
 comments to describe the functionality.  Microsoft VS Code also uses
 this format in their configuration files like settings.json,
 keybindings.json, launch.json, etc.
 .
 What this package offers
 .
 "JSONC for Go" offers ability to convert and unmarshal JSONC to pure
 JSON.  It also provides functionality to read JSONC file from disk and
 return JSONC and corresponding JSON encoding to operate on.  However, it
 only provides a one-way conversion.  That is, you can not generate JSONC
 from JSON.  Read documentation (DOCUMENTATION.md) for detailed examples.
